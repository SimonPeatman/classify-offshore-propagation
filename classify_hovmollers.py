#!/usr/bin/env python3

"""Manually classify each Hovmoller day"""

# Imports
import sys
from pathlib import Path
import shutil
import datetime
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.widgets
import iris
if tuple(map(int, iris.__version__.split('.'))) >= (3, 3):
    iris.FUTURE.datum_support = True  # Suppress warnings
import iris.plot as iplt

# Data locations
pcp_file = 'pcp.nc'
csv_file = 'saved_classifications.csv'
if Path(csv_file).is_file():
    shutil.copy(csv_file,
                csv_file + f'.{datetime.datetime.now():%Y%m%d_%H%M%S}.BACKUP')

# Set up plot
fig = plt.figure(figsize=(8, 6))
ax = fig.add_axes([0.075, 0.25, 0.9, 0.73])
ax_radios = fig.add_axes([0.075, 0.02, 0.475, 0.18])
ax_confirm = fig.add_axes([0.6, 0.02, 0.35, 0.18])
cmap_ = plt.get_cmap('GnBu')
colours = cmap_(np.linspace(0.3, 0.9, int(cmap_.N * 0.6)))
cmap = LinearSegmentedColormap.from_list('pcp', colours)
cmap.set_under('1')
cmap.set_over('#084081')
levels = [0.5, 1, 2, 4, 7, 11]
norm = matplotlib.colors.BoundaryNorm(levels, cmap.N)
xticks = range(-160, 81, 80)
xticklabels = [f'{xx}km'.replace('-', '$-$') for xx in xticks]
tticks = np.arange(5, 25, 3)
tlim = [23.5, 5]
tticklabels = [f'{(tt+7)%24:02d}LT' for tt in tticks]
labels = ['None of the below', 'Land DC - propagates offshore',
          'Land DC - doesn\'t propagate offshore', 'Dominated by large-scale']

class Control:

    def __init__(self, ax, units, fout):
        self.ax = ax
        self.units = units
        self.fout = fout
        self.hov = None
        self._radios_()
        self.count = 0

    def plot(self):
        self.ax.cla()
        iplt.contourf(self.hov, coords=['distance_onshore', 'hour_utc'],
                      cmap=cmap, levels=levels, norm=norm, extend='max',
                      axes=self.ax)
        self.ax.set_xticks(xticks)
        self.ax.set_xticklabels(xticklabels)
        self.ax.set_yticks(tticks)
        self.ax.set_yticklabels(tticklabels)
        self.ax.plot([0]*2, tlim, 'k', lw=0.5)
        self.ax.set_ylim(tlim)
        self._radios_()
        plt.draw()

    def confirm(self, event):
        date = self.units.num2date(self.hov.coord('date').points[0])
        value = self.radios.value_selected
        if value is None:
            return None
        fout.write(f'{name},{date:%Y/%m/%d},{value}\n')
        self.hov = next(hov_iter)
        self.plot()
        self.count += 1

    def _radios_(self):
        ax_radios.cla()
        self.radios = matplotlib.widgets.RadioButtons(ax_radios, labels,
                                                      active=None)

# Read in precip data and shuffle order
pcp = iris.load_cube(pcp_file)
iris.util.demote_dim_coord_to_aux_coord(pcp, 'date')
indices = list(range(pcp.shape[0]))
np.random.shuffle(indices)
pcp_shuffled = pcp[indices]

# Check whether CSV file has been updated
print()
resp = None
resp_prompt = 'Have you done "git pull origin master"? [y/n] '
while (resp := input(resp_prompt).lower()) not in ['y', 'n']:
    pass
if resp == 'n':
    sys.exit()

# Ask for name
name = input('What is your name? ')

# Create widget
hov_iter = pcp_shuffled.slices_over('date')
with open(csv_file, 'a') as fout:
    control = Control(ax, pcp.coord('date').units, fout)
    control.hov = next(hov_iter)
    confirm_button = matplotlib.widgets.Button(ax_confirm, 'Confirm')
    confirm_button.on_clicked(control.confirm)
    control.plot()
    plt.show()

print()
entries_str = 'entry' if control.count == 1 else 'entries'
print(f'Added {control.count} {entries_str} to {csv_file}')
if control.count:
    print('Please commit and push to update the online list')
